nama : Muhammad Habibi Ramadhan
1. Buat Database

CREATE DATABASE `myshop`;

2. Membuat Table di Dalam Database

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
)
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
)
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `items`
  ADD CONSTRAINT `items_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);
COMMIT;

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
)
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


3.Memasukkan Data pada Table

INSERT INTO `users` (`id`, `name`, `email`, `password`) VALUES (NULL, 'John doe', 'john@doe.com', 'john123'), (NULL, 'Jane Doe', 'jane@doe.com', ' jenita123'); 

INSERT INTO `categories` (`id`, `name`) VALUES (NULL, 'gadget'), (NULL, 'cloth'), (NULL, 'men'), (NULL, 'women'), (NULL, 'branded');

INSERT INTO `items` (`id`, `name`, `description`, `price`, `stock`, `category_id`) VALUES (NULL, 'Sumsang b50', 'hape keren dari merek sumsang', '4000000', '100', '1'), (NULL, 'Uniklooh', 'baju keren dari brand ternama', '500000', '50', '2'), (NULL, 'IMHO Watch', 'jam tangan anak yang jujur banget', '2000000', '10', '1'); 


4.Mengambil Data dari Database

a.Mengambil data users

SELECT `id`,`name`,`email` FROM `users` 

b.Mengambil data items

SELECT * FROM `items` WHERE `price` > 1000000

SELECT * FROM `items` WHERE `name` LIKE "%sang%" 

c.Menampilkan data items join dengan kategori

SELECT items.name, items.description, items.price, items.stock, items.category_id, categories.name as kategori FROM items INNER JOIN categories ON items.category_id = categories.id 


5.Mengubah Data dari Database

UPDATE `items` SET `price` = '2500000' WHERE `items`.`id` = 1; 



